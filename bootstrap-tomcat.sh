#!/bin/bash

sudo yum install -y wget java-1.8.0-openjdk git maven

git clone https://github.com/meirwah/spring-boot-tomcat-mysql-app
cd spring-boot-tomcat-mysql-app
mvn package
cd ..
sudo mv spring-boot-tomcat-mysql-app /opt
wget http://mirror.rise.ph/apache/tomcat/tomcat-8/v8.5.51/bin/apache-tomcat-8.5.51.tar.gz
tar xvzf apache-tomcat-8.5.51.tar.gz
sudo mv apache-tomcat-8.5.51 /opt
sudo ln -s /opt/apache-tomcat-8.5.51 /opt/tomcat
sudo chown tomcat: /opt/tomcat
sudo chown -R tomcat: /opt/apache-tomcat-8.5.51
sudo chown -R tomcat: /opt/spring-boot-tomcat-mysql-app
cat << EOF > tomcat.service
[Unit]
Description=Tomcat
After=syslog.target network.target

[Service]
SuccessExitStatus=143

User=tomcat
Group=tomcat

Environment="JAVA_HOME=/usr/lib/jvm/jre"
Environment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom"

Environment="CATALINA_HOME=/opt/tomcat/"
Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"

Environment="DB_URL=10.80.80.3"
Environment="DB_NAME=pearl-db-01"
Environment="DB_USER=test-user"
Environment="DB_USER_PASS=3NgyiIZ#B4QG5jWl"

ExecStart=/usr/bin/java -jar /opt/spring-boot-tomcat-mysql-app/target/spring-boot-sample-tomcat-1.1.5.RELEASE.jar
ExecStop=/opt/tomcat/bin/shutdown.sh

[Install]
WantedBy=multi-user.target
EOF
sudo mv tomcat.service /etc/systemd/system/tomcat.service
sudo systemctl enable tomcat.service
