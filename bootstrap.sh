#!/bin/bash

sudo apt -y update
sudo apt -y upgrade
sudo apt install -y git unzip
wget https://releases.hashicorp.com/packer/1.4.4/packer_1.4.4_linux_amd64.zip
unzip packer_1.4.4_linux_amd64.zip
sudo mv packer /usr/local/bin
wget https://releases.hashicorp.com/terraform/0.12.12/terraform_0.12.12_linux_amd64.zip
unzip terraform_0.12.12_linux_amd64.zip
sudo mv terraform /usr/local/bin
