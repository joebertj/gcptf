# GCP Terraform

This is Packer and Terraform on GCP

## Packer

### Images
Apache
Tomcat

### Operating Systems
Ubuntu
Centos 

## Terraform

### Version 11 and 12 differences
- `metadata {` to `metadata = {`
- `authorized_networks = [` to `authorized_netowrks {`

### Debug
`TF_LOG=DEBUG terraform apply`

## GCP

### APIs needed for Packer 
```
gcloud services enable sourcerepo.googleapis.com
gcloud services enable cloudapis.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable servicemanagement.googleapis.com
gcloud services enable storage-api.googleapis.com
gcloud services enable cloudbuild.googleapis.com
```
