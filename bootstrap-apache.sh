#!/bin/bash

sudo yum install -y httpd
echo "ProxyPass / http://tomcat.technopal.local:8080/" > proxypass.conf
echo "ProxyPassReverse / http://tomcat.technopal.local:8080/" >> proxypass.conf
sudo mv proxypass.conf /etc/httpd/conf.d/proxypass.conf
sudo restorecon /etc/httpd/conf.d/proxypass.conf
sudo setsebool -P httpd_can_network_connect on
sudo systemctl enable httpd
