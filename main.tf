provider "google" {
  credentials = file("container-254002-de1f6400a145.json")
  project     = "container-254002"
  region      = "us-central1"
}

resource "google_compute_instance" "apache" {
  name         = "apache"
  machine_type = "n1-standard-1"
  zone         = "us-west1-a"

  allow_stopping_for_update = true

  tags = ["apache", "test"]

  boot_disk {
    initialize_params {
      image   = "packer-1582870459"
      type    = "pd-standard"
      size    = "10"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    name = "test"
    ssh-keys = "joebert:${file("/home/joebert/.ssh/gcp.pub")}"
  }

}

resource "google_compute_instance" "tomcat" {
  name         = "tomcat"
  machine_type = "n1-standard-2"
  zone         = "us-west1-a"

  allow_stopping_for_update = true

  tags = ["tomcat", "test"]

  boot_disk {
    initialize_params {
      image   = "packer-1582871769"
      type    = "pd-standard"
      size    = "10"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    name = "test"
    ssh-keys = "joebert:${file("/home/joebert/.ssh/gcp.pub")}"
  }
}

resource "google_compute_firewall" "apache" {
 name    = "apache-firewall"
 network = "default"

 allow {
   protocol = "icmp"
 }

 allow {
   protocol = "tcp"
   ports    = ["80"]
 }

 source_ranges = ["0.0.0.0/0"]
 target_tags = ["apache"]
}

resource "google_compute_firewall" "tomcat" {
 name    = "tomcat-firewall"
 network = "default"

 allow {
   protocol = "icmp"
 }

 allow {
   protocol = "tcp"
   ports    = ["8080"]
 }

 source_ranges = ["10.0.0.0/8"]
 target_tags = ["tomcat"]
}

/*
data "null_data_source" "auth_netw_mysql_allowed_pri" {
  count = 1 #"${length(google_compute_instance.tomcat.*.self_link)}"

  inputs = {
    name  = "default-${count.index + 1}"
    value = "${element(google_compute_instance.tomcat.*.network_interface.0.access_config.0.nat_ip, count.index)}"
  }
}

data "null_data_source" "auth_netw_mysql_allowed_pub" {
  count = 1

  inputs = {
    name  = "onprem-${count.index + 1}"
    value = "${element(list("49.145.104.20"), count.index)}"
  }
}

resource "google_sql_database_instance" "master" {
  name = "test-instance-02"
  database_version = "MYSQL_5_7"
  region = "us-west1"

  settings {
    #tier = "db-n1-highmem-2"
    tier = "db-f1-micro"
    ip_configuration {
      ipv4_enabled = true
      authorized_networks = [
        "${data.null_data_source.auth_netw_mysql_allowed_pri.*.outputs}",
        "${data.null_data_source.auth_netw_mysql_allowed_pub.*.outputs}"
      ]
    }
  }
}

resource "google_sql_database" "database" {
    name = "test-db-02"
    instance = "${google_sql_database_instance.master.name}"
}

resource "google_sql_user" "users" {
  name     = "test-user"
  instance = "${google_sql_database_instance.master.name}"
  host     = "49.145.104.20"
  password = "3NgyiIZ#B4QG5jWl"
}
*/
